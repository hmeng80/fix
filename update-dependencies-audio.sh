#!/bin/sh

opkg install "gstreamer1.0-plugins-good"
opkg install "gstreamer1.0-plugins-base"
opkg install "gstreamer1.0-plugins-bad"
opkg install "gstreamer1.0-plugins-ugly"
opkg install alsa-conf
opkg install alsa-plugins
opkg install alsa-state
opkg install enigma2
opkg install libasound2  
opkg install libc6 
opkg install libgcc1 
opkg install libstdc++6 
opkg install python3-core
opkg install python3-cryptography


#check python version
python=$(python -c "import platform; print(platform.python_version())")
sleep 1;
case $python in 
2.7.18)
opkg install libavcodec58
opkg install libavformat58
opkg install libpython2.7-1.0
;;
3.9.9)
opkg install libavcodec58
opkg install libavformat58
opkg install libpython3.9-1.0
;;
3.10.4)
opkg install libavcodec60
opkg install libavformat60
opkg install libpython3.10-1.0
;;
3.11.0|3.11.1|3.11.2|3.11.3|3.11.4|3.11.5|3.11.6)
opkg install libavcodec60
opkg install libavformat60
opkg install libpython3.11-1.0
;;
3.12.0|3.12.1|3.12.2|3.12.3|3.12.4|3.12.5|3.12.6)
opkg install libavcodec60
opkg install libavformat60
opkg install libpython3.12-1.0
;;
*)
echo ""
sleep 3
;;
esac